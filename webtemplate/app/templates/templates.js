'use strict';

angular.module('webTemplate.templates', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/templates', {
    templateUrl: 'templates/templates.html',
    controller: 'TemplateCtrl'
  })
  .when('/templates/:templateId', {
    templateUrl: 'templates/templates-details.html',
    controller: 'TemplateDetailsCtrl'
  });
}])

.controller('TemplateCtrl', ['$scope', '$http', function($scope, $http) {
	$http.get('json/templates.json').success(function(res) {
		$scope.templates = res;
	});
}])

.controller('TemplateDetailsCtrl', ['$scope', '$routeParams', '$http', '$filter', function($scope, $routeParams, $http, $filter) {
	var templateId = $routeParams.templateId;
	$http.get('json/templates.json').success(function(res) {
		$scope.template = $filter('filter')(res, function(d) {
			return d.id == templateId;
		})[0];
		$scope.mainImage = $scope.template.images[0].name;
	});
	
	$scope.setImage = function(image) {
		console.log(image);
		$scope.mainImage = image.name;
	}
}]);
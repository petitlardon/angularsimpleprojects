'use strict';

// Declare app level module which depends on views, and components
angular.module('webTemplate', [
  'ngRoute',
  'webTemplate.view1',
  'webTemplate.view2',
  'webTemplate.templates'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/templates'});
}]);

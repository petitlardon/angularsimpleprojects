'use strict';

angular.module('facebookApp.facebook', ['ngRoute', 'ngFacebook'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/facebook', {
    templateUrl: 'facebook/facebook.html',
    controller: 'FacebookCtrl'
  });
}])
										
.config( function( $facebookProvider ) {
  $facebookProvider.setAppId('783483448465299');
  $facebookProvider.setPermissions("email, public_profile, user_posts, publish_actions, user_photos");
})
										
.run( function( $rootScope ) {
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
})

.controller('FacebookCtrl', ['$scope', '$facebook', function($scope, $facebook) {
	$scope.isLoggedIn = false;
	
	$scope.login = function() {
		$facebook.login().then(function() {
			$scope.isLoggedIn = true;
			refresh();
		});
	}
	
	$scope.logout = function() {
		$facebook.logout().then(function() {
			$scope.isLoggedIn = false;
			refresh();
		});
	}
	
	function refresh() {
		$facebook.api("me?fields=name,email,gender,locale,link").then(function(res) {
			$scope.welcomeMes = "Welcome " + res.name;
			$scope.isLoggedIn = true;
			$scope.userInfos = res;
			
			$facebook.api("/me/picture").then(function(res) {
					console.log(res);
				$scope.picture = res.data.url;
			}); 
			
			$facebook.api("/me/permissions").then(function(res) {
				$scope.permissions = res.data;
			}); 
			
			$facebook.api("/me/posts?fields=message,created_time,attachments&limit=5").then(function(res) {
				$scope.posts = res.data;
			}); 
			
		}, 
		function(err) {
			$scope.welcomeMes = "Please log in";
		});
	}
	
	$scope.postStatus = function() {
		var postMessage = this.postMessage;
		$facebook.api('/me/feed', 'post', {message : postMessage}).then(function(res) {
			$scope.msg = 'Post done';
			refresh();
		});
	}
	refresh();
}]);
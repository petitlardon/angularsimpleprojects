'use strict';

angular.module('facebookApp.version', [
  'facebookApp.version.interpolate-filter',
  'facebookApp.version.version-directive'
])

.value('version', '0.1');

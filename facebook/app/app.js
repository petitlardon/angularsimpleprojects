'use strict';

// Declare app level module which depends on views, and components
angular.module('facebookApp', [
  'ngRoute',
  'facebookApp.facebook'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/facebook'});
}]);
